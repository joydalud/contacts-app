import './App.css';
import DataTable from './components/data-table/data-table.component';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="wrapper">
          <span className="logo"></span>
        </div>
      </header>
      <DataTable />
    </div>
  );
}

export default App;
