import React, {useCallback, useState} from 'react';
import ContactForm from '../form/form.component';
import './data-table.styles.css';
import 'antd/dist/antd.css';
import {Table, Form, Input, Button, Select, Space, message} from 'antd';
import { EditFilled, DeleteFilled } from '@ant-design/icons';
import CONTACT_DATA from './contacts.data';
const { Option } = Select;

const contactInfo = {
    name: '',
    email: '',
    address: '',
    phone: ''
}
function DataTable() {
    const [form] = Form.useForm();
    const [contacts, setContacts] = useState(CONTACT_DATA);
    const [searchText, setSearchText] = useState(null);
    const [searchBy, setSearchBy] = useState('name');
    const [showForm, setShowForm] = useState(false);
    const [editContact, setEditContact] = useState(false);
    const [selectedRow, setSelectedRow] = useState(contactInfo);
    const [editId, setEditId] = useState(null);
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.length - b.name.length
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            sorter: (a, b) => a.email.length - b.email.length
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address'
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone'
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
              <Space size="middle">
                <a 
                    onClick={() => {
                        setEditContact(true)
                        setShowForm(true)
                        setSelectedRow(record)
                        setEditId(record.id)
                    }}
                ><EditFilled /></a>
                <a
                    onClick={() => {
                        deleteContact(record.id)
                    }}
                ><DeleteFilled /></a>
              </Space>
            ),
          },        
    ];

    const searchList = () => {
        let col;

        const list = CONTACT_DATA.filter((item) => {
            searchBy === 'name' ? col = item.name.toLowerCase() : col = item.email.toLowerCase();
            return col.includes(searchText.toLowerCase())
        });
        setContacts(list);
    };
    const resetList = () => {
        setContacts(CONTACT_DATA);
        setSearchText(null);
		form.setFieldsValue({name: null})        
    };
    const cancelContactForm = () => {
        setSelectedRow(contactInfo);
        setShowForm(false);
    }
    const deleteContact = (id) => {
        let updatedContacts = contacts.filter((item) => { 
            return item.id !== id;
        });
        setContacts(updatedContacts);
        message.success('contact has been deleted.');
    }
    const submitContactForm = useCallback((form) => {
        let list;
        if(!editContact){
            const newContact = {
                id: CONTACT_DATA.length+1,
                ...form.getFieldsValue()
            }
    
            list = [
                newContact,
                ...CONTACT_DATA
            ];
            console.log(list);
            setContacts(list);
        } else {
            console.log(form.getFieldsValue());
            const editRow = contacts.filter((item) => {
                return item.id === editId
            });
            const updatedContacts = contacts.filter((item) => {
                return item.id !== editId
            });
            const index = contacts.map(e => e.id).indexOf(editId);
            let obj = contacts[index];
            obj = {
              ...editRow[0],
              ...form.getFieldsValue()  
            }

            list = [
                obj,
                ...updatedContacts
            ];
            setContacts(list);
        }
        message.success('contact information is saved');
        setShowForm(false);
    })
    return (
        <>
            <div className="table-form">
                <Form form={form} layout="inline">
                    <Form.Item className="form-item" name="searchBy" label="Search By">
                    <Select 
                        defaultValue="name"
                        onChange={(e) => {
                            setSearchBy(e)
                        }}
                    >
                        <Option value="name">Name</Option>
                        <Option value="email">Email</Option>
                    </Select>
                    </Form.Item>
                    <Form.Item className="form-item" name="name">
                        <Input 
                            onChange={(e) => {
                                setSearchText(e.target.value)
                            }}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={searchList}>Search</Button>
                        <Button 
                            type="primary" 
                            onClick={() => resetList()}
                            style={{marginLeft: '10px'}}
                        >
                        Reset
                        </Button>
                    </Form.Item>
                </Form>
                <div className="contactButtons">
                    <Button type="primary" onClick={() => {
                            setShowForm(true)
                            setEditContact(false)
                            setSelectedRow(contactInfo)
                            }}>Add Contact</Button>
                </div>
            </div>  
            <div className="table-wrapper">
                <Table columns={columns} dataSource={contacts} />
            </div>
            <ContactForm 
                showForm={showForm}
                editContact={editContact}
                cancelContactForm={cancelContactForm}
                submitContactForm={submitContactForm}
                selectedRow={selectedRow}          
            />
        </>
    );
  }

export default DataTable;