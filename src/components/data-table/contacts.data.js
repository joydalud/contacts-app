const CONTACT_DATA = [
    {
        "id": 1,
        "name": "Leanne Graham",
        "email": "Sincere@april.biz",
        "address": "Kulas Light Apt. 556 Gwenborough",
        "phone": "1-770-736-8031" 
    },
    {
        "id": 2,
        "name": "Ervin Howell",
        "email": "Shanna@melissa.tv",
        "address": "Victor Plains Suite 879 Wisokyburgh",
        "phone": "010-692-6593"  
    },
    {
        "id": 3,
        "name": "Clementine Bauch",
        "email": "Nathan@yesenia.net",
        "address": "Douglas Extension Suite 847 McKenziehaven",
        "phone": "1-463-123-4447"
    },    
    {
        "id": 4,
        "name": "Patricia Lebsack",
        "email": "Julianne.OConner@kory.org",
        "address": "Hoeger Mall Apt. 692 South Elvis",
        "phone": "493-170-9623"
    },
    {
        "id": 5,
        "name": "Chelsey Dietrich",
        "email": "Lucio_Hettinger@annie.ca",
        "address": "Skiles Walks Suite 351 Roscoeview",
        "phone": "(254)954-1289"
    },
    {
        "id": 6,
        "name": "Mrs. Dennis Schulist",
        "email": "Karley_Dach@jasper.info",
        "address": "Norberto Crossing Apt. 950 South Christy",
        "phone": "1-477-935-8478"
    },
    {
        "id": 7,
        "name": "Kurtis Weissnat",
        "email": "Telly.Hoeger@billy.biz",
        "address": "Rex Trail Suite 280 Howemouth",
        "phone": "210.067.6132"
    },
    {
        "id": 8,
        "name": "Nicholas Runolfsdottir V",
        "email": "Sherwood@rosamond.me",
        "address": "Ellsworth Summit Suite 729 Aliyaview",
        "phone": "586.493.6943"
    },
    {
        "id": 9,
        "name": "Glenna Reichert",
        "email": "Chaim_McDermott@dana.io",
        "address": "Dayna Park Suite 449 Bartholomebury",
        "phone": "(775)976-6794"
    },
    {
        "id": 10,
        "name": "Clementina DuBuque",
        "email": "Rey.Padberg@karina.biz",
        "address": "Kattie Turnpike Suite 198 Lebsackbury",
        "phone": "024-648-3804"
    },
    {
        "id": 11,
        "name": "Mark Reyes",
        "email": "mark@april.biz",
        "address": "Kulas Light Apt. 556 Gwenborough",
        "phone": "1-770-736-8031" 
    },
    {
        "id": 12,
        "name": "Jane Doe",
        "email": "jane@melissa.tv",
        "address": "Victor Plains Suite 879 Wisokyburgh",
        "phone": "010-692-6593"  
    },
    {
        "id": 13,
        "name": "Jill Valentine",
        "email": "jill@yesenia.net",
        "address": "Douglas Extension Suite 847 McKenziehaven",
        "phone": "1-463-123-4447"
    },    
    {
        "id": 14,
        "name": "Chris Redfield",
        "email": "chris@kory.org",
        "address": "Hoeger Mall Apt. 692 South Elvis",
        "phone": "493-170-9623"
    },
    {
        "id": 15,
        "name": "Leon Howard",
        "email": "leon@annie.ca",
        "address": "Skiles Walks Suite 351 Roscoeview",
        "phone": "(254)954-1289"
    },
    {
        "id": 16,
        "name": "Bart Simpson",
        "email": "bart@jasper.info",
        "address": "Norberto Crossing Apt. 950 South Christy",
        "phone": "1-477-935-8478"
    },
    {
        "id": 17,
        "name": "Joey Fatone",
        "email": "joey@billy.biz",
        "address": "Rex Trail Suite 280 Howemouth",
        "phone": "210.067.6132"
    },
    {
        "id": 18,
        "name": "Nora Sesame",
        "email": "nora@rosamond.me",
        "address": "Ellsworth Summit Suite 729 Aliyaview",
        "phone": "586.493.6943"
    },
    {
        "id": 19,
        "name": "Amara Tiick",
        "email": "amara@dana.io",
        "address": "Dayna Park Suite 449 Bartholomebury",
        "phone": "(775)976-6794"
    },
    {
        "id": 20,
        "name": "Celestine Diaz",
        "email": "celestine@karina.biz",
        "address": "Kattie Turnpike Suite 198 Lebsackbury",
        "phone": "024-648-3804"
    },
    {
        "id": 21,
        "name": "Kris Allen",
        "email": "kris@april.biz",
        "address": "Kulas Light Apt. 556 Gwenborough",
        "phone": "1-770-736-8031" 
    },
    {
        "id": 22,
        "name": "Sarah Hutton",
        "email": "sarah@melissa.tv",
        "address": "Victor Plains Suite 879 Wisokyburgh",
        "phone": "010-692-6593"  
    },
    {
        "id": 23,
        "name": "Tere Gonzales",
        "email": "tere@yesenia.net",
        "address": "Douglas Extension Suite 847 McKenziehaven",
        "phone": "1-463-123-4447"
    },    
    {
        "id": 24,
        "name": "Pedro Solis",
        "email": "pedro@kory.org",
        "address": "Hoeger Mall Apt. 692 South Elvis",
        "phone": "493-170-9623"
    },
    {
        "id": 25,
        "name": "Jose Cruz",
        "email": "jose@annie.ca",
        "address": "Skiles Walks Suite 351 Roscoeview",
        "phone": "(254)954-1289"
    },
    {
        "id": 26,
        "name": "Britney Spears",
        "email": "britney@jasper.info",
        "address": "Norberto Crossing Apt. 950 South Christy",
        "phone": "1-477-935-8478"
    },
    {
        "id": 27,
        "name": "Justin Timberlake",
        "email": "justin@billy.biz",
        "address": "Rex Trail Suite 280 Howemouth",
        "phone": "210.067.6132"
    },
    {
        "id": 28,
        "name": "Shawn Wayans",
        "email": "shawn@rosamond.me",
        "address": "Ellsworth Summit Suite 729 Aliyaview",
        "phone": "586.493.6943"
    },
    {
        "id": 29,
        "name": "Bruce Wayne",
        "email": "bruce@dana.io",
        "address": "Dayna Park Suite 449 Bartholomebury",
        "phone": "(775)976-6794"
    },
    {
        "id": 30,
        "name": "Clark Kent",
        "email": "clark_kent@karina.biz",
        "address": "Kattie Turnpike Suite 198 Lebsackbury",
        "phone": "024-648-3804"
    }        
];

export default CONTACT_DATA;