import React, {useEffect} from 'react';
import 'antd/dist/antd.css';
import { Modal, Form, Input, Button } from 'antd';

const layout = {
    labelCol: {
        span: 4,
    },
    wrapperCol: {
        span: 16,
    },
};
const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!'
    }
  };
const ContactForm = (props) => {
    const [form] = Form.useForm();

    useEffect(() => {
        form.setFieldsValue({
          ...props.selectedRow
        });
    }, [props.selectedRow]);

    return(
    <Modal
        title={props.editContact ? "Edit Contact":"Add Contact"}
        centered
        visible={props.showForm}
        onOk={() => form.submit()}
        onCancel={props.cancelContactForm}
        width={500}
    >
    <Form {...layout} form={form} onFinish={()=>props.submitContactForm(form)} validateMessages={validateMessages}>
      <Form.Item
        name="name"
        label="Name"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="email"
        label="Email"
        rules={[
          {
            required: true,
            type: 'email',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="address"
        label="Address"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="phone"
        label="Phone"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>            
    </Form>
    </Modal>
    )
}

export default ContactForm;